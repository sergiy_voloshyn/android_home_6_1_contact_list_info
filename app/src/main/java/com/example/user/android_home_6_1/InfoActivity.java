package com.example.user.android_home_6_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.user.android_home_6_1.MainActivity.EMAIL;
import static com.example.user.android_home_6_1.MainActivity.NAME;
import static com.example.user.android_home_6_1.MainActivity.PHONE;
import static com.example.user.android_home_6_1.MainActivity.ADDRESS;

public class InfoActivity extends AppCompatActivity {
    @BindView(R.id.name)
    TextView viewName;
    @BindView(R.id.email)
    TextView viewEmail;
    @BindView(R.id.phone)
    TextView viewPhone;
    @BindView(R.id.address)
    TextView viewAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        ButterKnife.bind(this);

        viewName.setText(getIntent().getExtras().getString(NAME));
        viewEmail.setText(getIntent().getExtras().getString(EMAIL));
        viewPhone.setText(getIntent().getExtras().getString(PHONE));
        viewAddress.setText(getIntent().getExtras().getString(ADDRESS));
    }
}
