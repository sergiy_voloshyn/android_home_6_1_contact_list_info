package com.example.user.android_home_6_1;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by user on 21.01.2018.
 */

public class ContactRecyclerAdapter extends RecyclerView.Adapter<ContactRecyclerAdapter.ViewHolder> {

    private ArrayList<Contact> list;
    private Context context;
    private OnContactClick onContactClick;


    public ContactRecyclerAdapter(ArrayList<Contact> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void addToPosition(Contact contact, int position) {
        list.add(position, contact);
        notifyItemInserted(position);
    }


    public void setOnContactClick(OnContactClick onContactClick) {
        this.onContactClick = onContactClick;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateData(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    interface OnContactClick {
        void onItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.photo)
        ImageView photo;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.email)
        TextView email;

        Contact contact;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateData(Contact contact) {
            this.contact = contact;
            photo.setImageResource(contact.getPhoto());
            name.setText(contact.getName());
            email.setText(contact.getEmail());
        }

        @OnClick(R.id.root)
        void onItemClick() {
            if (onContactClick != null) {
                onContactClick.onItemClick(getAdapterPosition());
            }
        }

    }
}
