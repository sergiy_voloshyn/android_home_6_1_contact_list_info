package com.example.user.android_home_6_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/*

1. Создать список контактов. Каждый контакт содержит имя, email, адрес и
телефон иконку (или фотку). Список содержит только иконку, имя и email контакта.
При нажатии на контакт - он должен открываться в новом activity с подробной
 о нем информацией. Использовать ListView.
//first activity
Intent mIntent = new Intent(this, SecondActivity.class);
mIntent.putExtra(key, value); //добавляем в intent значение и ключ к нему
//second activity
String value = getIntent().getExtras().getString(key); //получаем добавленное значение,
указав ключ
1.1* Добавить возможность добавлять контакты в список на первом экране.

 */

public class MainActivity extends AppCompatActivity {

    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String PHONE = "PHONE";
    public static final String ADDRESS = "ADDRESS";

    @BindView(R.id.list)
    RecyclerView list;
    ContactRecyclerAdapter adapter;
    ArrayList<Contact> contactList=Generator.generate();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        adapter = new ContactRecyclerAdapter(contactList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);
        adapter.setOnContactClick(new ContactRecyclerAdapter.OnContactClick() {

            @Override
            public void onItemClick(int position) {

                Intent showInfo =new Intent( MainActivity.this,InfoActivity.class);

                showInfo.putExtra(NAME, contactList.get(position).getName());
                showInfo.putExtra(EMAIL, contactList.get(position).getEmail());
                showInfo.putExtra(PHONE, contactList.get(position).getPhone());
                showInfo.putExtra(ADDRESS, contactList.get(position).getAddress());

                startActivity(showInfo);

            }
        });
    }



    @OnClick(R.id.buttonAdd)
    public void onClickButton() {
        Contact contact = new Contact();
        adapter.addToPosition(contact, 0);
        list.smoothScrollToPosition(0);

    }

}
